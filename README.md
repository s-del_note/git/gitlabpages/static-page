# GitLabPages で静的 Web ページ
[https://s-del_settings.gitlab.io/gitlab/gitlabpages](https://s-del_settings.gitlab.io/gitlab/gitlabpages)  
細かい手順は公式の [GitLab Pagesで静的ウェブサイトを公開する方法 | GitLab.JP](https://www.gitlab.jp/blog/2020/06/29/how-to-use-gitlab-pages/) に載っている

## ざっくりした公開手順
1. 新規プロジェクト作成
1. テンプレートに `Pages/Plane HTML` を選択
1. プロジェクトの `CI / CD` から `パイプライン` を選択
1. `パイプライン実行` ボタンでしばらくすると公開開始
1. `設定` の `Pages` を選択
1. 表示されている URL でアクセス

## 参考
- [GitLab Pagesでウェブサイトにアクセス制限をかける方法 | GitLab.JP](https://www.gitlab.jp/blog/2020/07/02/how-to-restrict-access-by-gitlab-pages/)
- [GitLabのユーザー名・グループ名を決める際の注意点 | GitLab.JP](https://www.gitlab.jp/blog/2020/06/24/why-shouldnt-use-dot-in-usernames/)
